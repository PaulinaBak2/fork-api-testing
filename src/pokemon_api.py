import requests


class Pokeapi:
    base_url = "https://pokeapi.co/api/v2"
    pokemon_endpoint = "/pokemon"
    pokemon_shape_endpoint = "/pokemon-shape"

    def get_all_pokemons(self, params=None):
        response = requests.get(self.base_url + self.pokemon_endpoint, params=params)
        assert response.status_code == 200
        return response

    def get_all_pokemon_shapes(self, params=None):
        response = requests.get(self.base_url + self.pokemon_shape_endpoint, params=params)
        assert response.status_code == 200
        return response

    def get_any_name_from_pokemon_shapes(self, count):
        response = self.get_all_pokemon_shapes().json()
        name = response["results"][count-1]["name"]
        return name

    def get_pokemon_shape_by_name(self, count):
        name = self.get_any_name_from_pokemon_shapes(count)
        response = requests.get(f"{self.base_url + self.pokemon_shape_endpoint}/{name}")
        return response




